#!/usr/bin/env bash

if ! [ -s "pom.xml" ]
then
    echo "$0 should be run at the project root"
    exit
fi

main_classes=($(rg -l -tjava "public static void main" src))
main_classes_count=${#main_classes[@]}

if (( main_classes_count > 1 ))
then
    echo "There are more than one main class:"
    echo "Please choose one:"

    for key in "${!main_classes[@]}"
    do
	printf "\t%s: %s\n" $key ${main_classes[key]}
    done

    read selected

    if (( selected < 0 || selected >= main_classes_count ))
    then
	echo "Invalid choice"
	exit
    fi

    MAINCLASS=${main_classes[$selected]}
else
    MAINCLASS=${main_classes[0]}
fi

if [ "$MAINCLASS" = "" ]
then
    echo "Cannot find the main class"
    exit
fi

MAINCLASS=$(echo $MAINCLASS | sed 's#src/main/java/##' | sed 's#\.java##' | tr '/' '.')

artifact=$( echo '${project.artifactId}-${project.version}.jar' |  mvn org.apache.maven.plugins:maven-help-plugin:3.2.0:evaluate -q -DforceStdout )

mvn -q package

if ! [ -s "./target/${artifact}" ]
then
    echo "mvn package is executed, but no artifact was generated"
    exit
fi

dep_classpath=$(mvn dependency:build-classpath | grep -v '^\[' | tr -d '\n')

CLASSPATH="./target/$artifact:$dep_classpath"

java -cp $CLASSPATH $MAINCLASS

