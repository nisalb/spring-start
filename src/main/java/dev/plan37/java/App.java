package dev.plan37.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        var context = new AnnotationConfigApplicationContext(ProjectConfig.class);

        Parrot p = new Parrot();
        p.setName("Duke");

        context.registerBean("duke", Parrot.class, () -> p);

        Parrot pFromCtx = context.getBean(Parrot.class);
        System.out.println(pFromCtx.getName());

        context.close();
    }
}
