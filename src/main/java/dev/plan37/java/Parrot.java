package dev.plan37.java;

class Parrot
{
    private String name;

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }
}
